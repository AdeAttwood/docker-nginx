# Copyright 2021 Practically.io All rights reserved
#
# Use of this source is governed by a BSD-style
# licence that can be found in the LICENCE file or at
# https://www.practically.io/copyright/
#
# ** AUTO GENERATED DO NOT EDIT **
#
# Nginx template was created from /docker-nginx/templates/fmp.tpl
# for more info see https://gitlab.com/adeattwood/docker-nginx

server {
    server_name ${SERVER_NAME};
    ${LISTEN}
    ${CERT}

    charset utf-8;
    client_max_body_size 128M;

    root ${WEB_ROOT};
    index index.html index.php;

    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }

    location ~ \.php$ {
        include fastcgi_params;

        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_pass ${UPSTREAM};

        try_files $uri = 404;
    }
}
