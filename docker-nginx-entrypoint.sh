#!/usr/bin/env bash
#
# Copyright 2021 Practically.io All rights reserved
#
# Use of this source is governed by a BSD-style
# licence that can be found in the LICENCE file or at
# https://www.practically.io/copyright/

set -e;

run_main() {
	export WEB_ROOT=${WEB_ROOT:=/var/www/html}
	export UPSTREAM=${UPSTREAM:=fpm:9000}
	export SERVER_NAME=${SERVER_NAME:=localhost}

	if [[ ! -z $TLS_CERT ]] && [[ ! -z $TLS_KEY ]]; then
		export LISTEN="
	listen [::]:${PORT:=443} ssl http2;
	listen ${PORT:=443} ssl http2;"
		export CERT="
	ssl_certificate ${TLS_CERT};
	ssl_certificate_key ${TLS_KEY};"
	else
		export LISTEN="
	listen ${PORT:=80};"
		export CERT=""
	fi

	envsubst '
	${WEB_ROOT}
	${UPSTREAM}
	${SERVER_NAME}
	${LISTEN}
	${CERT}' </docker-nginx/templates/${TEMPLATE:=static}.tpl 
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
	run_main > /etc/nginx/conf.d/default.conf
fi
