FROM nginx

COPY templates /docker-nginx/templates
COPY docker-nginx-entrypoint.sh /docker-entrypoint.d/docker-nginx-entrypoint.sh
