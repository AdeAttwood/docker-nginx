# Docker Nginx

Nginx docker image configurable via environment variables

| Variable    | Default            | Description                                     |
| ----------- | ------------------ | ----------------------------------------------- |
| WEB_ROOT    | /var/www/html      | The document root the we server will serve from |
| UPSTREAM    | fpm:9000           | The upstream server to proxy to                 |
| SERVER_NAME | localhost          | The host name to serve from                     |
| PORT        | 80 or 443 if certs | The port to listen on                           |
| TLS_CERT    |                    | The path to the tls certificate                 |
| TLS_KEY     |                    | The path to the tls private key                 |
